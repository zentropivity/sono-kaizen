#!/usr/bin/fish

argparse 'f/flavor' 'a/app' -- $argv
or return

set flavor $argv[1]
set app $argv[2]

set flavors default shusia andromeda atlantis maia espresso

mkdir -p ../build

function palette
    echo Using flavor: $flavor
    switch $flavor
        case default
            set -g black       "#181819"
            set -g bg0         "#2c2e34"
            set -g bg1         "#33353f"
            set -g bg2         "#363944"
            set -g bg3         "#3b3e48"
            set -g bg4         "#414550"
            set -g bg_red      "#ff6077"
            set -g diff_red    "#55393d"
            set -g bg_green    "#a7df78"
            set -g diff_green  "#394634"
            set -g bg_blue     "#85d3f2"
            set -g diff_blue   "#354157"
            set -g diff_yellow "#4e432f"
            set -g fg          "#e2e2e3"
            set -g red         "#fc5d7c"
            set -g orange      "#f39660"
            set -g yellow      "#e7c664"
            set -g green       "#9ed072"
            set -g blue        "#76cce0"
            set -g purple      "#b39df3"
            set -g grey        "#7f8490"
            set -g grey_dim    "#595f6f"
        case shusia
            set -g black       "#1a181a"
            set -g bg0         "#2d2a2e"
            set -g bg1         "#37343a"
            set -g bg2         "#3b383e"
            set -g bg3         "#423f46"
            set -g bg4         "#49464e"
            set -g bg_red      "#ff6188"
            set -g diff_red    "#55393d"
            set -g bg_green    "#a9dc76"
            set -g diff_green  "#394634"
            set -g bg_blue     "#78dce8"
            set -g diff_blue   "#354157"
            set -g diff_yellow "#4e432f"
            set -g fg          "#e3e1e4"
            set -g red         "#f85e84"
            set -g orange      "#ef9062"
            set -g yellow      "#e5c463"
            set -g green       "#9ecd6f"
            set -g blue        "#7accd7"
            set -g purple      "#ab9df2"
            set -g grey        "#848089"
            set -g grey_dim    "#605d68"
        case andromeda
            set -g black       "#181a1c"
            set -g bg0         "#2b2d3a"
            set -g bg1         "#333648"
            set -g bg2         "#363a4e"
            set -g bg3         "#393e53"
            set -g bg4         "#3f445b"
            set -g bg_red      "#ff6188"
            set -g diff_red    "#55393d"
            set -g bg_green    "#a9dc76"
            set -g diff_green  "#394634"
            set -g bg_blue     "#77d5f0"
            set -g diff_blue   "#354157"
            set -g diff_yellow "#4e432f"
            set -g fg          "#e1e3e4"
            set -g red         "#fb617e"
            set -g orange      "#f89860"
            set -g yellow      "#edc763"
            set -g green       "#9ed06c"
            set -g blue        "#6dcae8"
            set -g purple      "#bb97ee"
            set -g grey        "#7e8294"
            set -g grey_dim    "#5a5e7a"
        case atlantis
            set -g black       "#181a1c"
            set -g bg0         "#2a2f38"
            set -g bg1         "#333846"
            set -g bg2         "#373c4b"
            set -g bg3         "#3d4455"
            set -g bg4         "#424b5b"
            set -g bg_red      "#ff6d7e"
            set -g diff_red    "#55393d"
            set -g bg_green    "#a5e179"
            set -g diff_green  "#394634"
            set -g bg_blue     "#7ad5f1"
            set -g diff_blue   "#354157"
            set -g diff_yellow "#4e432f"
            set -g fg          "#e1e3e4"
            set -g red         "#ff6578"
            set -g orange      "#f69c5e"
            set -g yellow      "#eacb64"
            set -g green       "#9dd274"
            set -g blue        "#72cce8"
            set -g purple      "#ba9cf3"
            set -g grey        "#828a9a"
            set -g grey_dim    "#5a6477"
        case maia
            set -g black       "#1c1e1f"
            set -g bg0         "#273136"
            set -g bg1         "#313b42"
            set -g bg2         "#353f46"
            set -g bg3         "#3a444b"
            set -g bg4         "#414b53"
            set -g bg_red      "#ff6d7e"
            set -g diff_red    "#55393d"
            set -g bg_green    "#a2e57b"
            set -g diff_green  "#394634"
            set -g bg_blue     "#7cd5f1"
            set -g diff_blue   "#354157"
            set -g diff_yellow "#4e432f"
            set -g fg          "#e1e2e3"
            set -g red         "#f76c7c"
            set -g orange      "#f3a96a"
            set -g yellow      "#e3d367"
            set -g green       "#9cd57b"
            set -g blue        "#78cee9"
            set -g purple      "#baa0f8"
            set -g grey        "#82878b"
            set -g grey_dim    "#55626d"
        case espresso
            set -g black       "#1f1e1c"
            set -g bg0         "#312c2b"
            set -g bg1         "#393230"
            set -g bg2         "#413937"
            set -g bg3         "#49403c"
            set -g bg4         "#4e433f"
            set -g bg_red      "#fd6883"
            set -g diff_red    "#55393d"
            set -g bg_green    "#adda78"
            set -g diff_green  "#394634"
            set -g bg_blue     "#85dad2"
            set -g diff_blue   "#354157"
            set -g diff_yellow "#4e432f"
            set -g fg          "#e4e3e1"
            set -g red         "#f86882"
            set -g orange      "#f08d71"
            set -g yellow      "#f0c66f"
            set -g green       "#a6cd77"
            set -g blue        "#81d0c9"
            set -g purple      "#9fa0e1"
            set -g grey        "#90817b"
            set -g grey_dim    "#6a5e59"
        case '*'
            echo Unknown flavor: $flavor!
            echo Available flavors: $flavors
            return 1
    end
end

function generate
    if test -e ./generators/$app.fish
        echo Generating for $app
        source ./generators/$app.fish
    else
        echo No generator for app: $app!
        return
    end
end

if test $flavor = "all"
    for f in $flavors
        set flavor $f
        palette
        generate
    end
else
    if palette
        generate
    end
end
