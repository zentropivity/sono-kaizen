#!/usr/bin/fish
mkdir -p ./build/helix

echo "# Sono Kaizen $flavor theme for the Helix editor
# Author: Zentropivity<zentropivity@pm.me>
# Repo: TODO
# Version: 0.1.0
# Generated from the original colors: https://github.com/sainnhe/sonokai
'info' = { fg = 'fg', bg = 'black' }
'hint' = { fg = 'yellow', bg = 'black', modifiers = ['bold'] }
'warning' = { fg = 'orange', bg = 'black', modifiers = ['bold'] }
'error' = { fg = 'red', bg = 'black', modifiers = ['bold'] }
'diagnostic' = { modifiers = ['underlined'] }
'diagnostic.hint' = { modifiers = ['underlined'] }
# 'diagnostic.info' = { fg = 'black', bg = 'yellow' }
# 'diagnostic.warning' = { fg = 'black', bg = 'orange' }
# 'diagnostic.error' = { fg = 'black', bg = 'bg_red' }
'diagnostic.warning' = { modifiers = ['bold', 'underlined'] }
'diagnostic.error' = { modifiers = ['bold', 'underlined'] }
'ui.background' = { bg = 'bg0' }
'ui.window' = { fg = 'black', bg = 'bg1' }
'ui.gutter' = { bg = 'bg1' }
'ui.text' = { fg = 'fg' }
'ui.text.focus' = { fg = 'yellow', modifiers = ['bold'] }
'ui.text.info' = { fg = 'orange', bg = 'bg1' }
'ui.linenr' = { fg = 'grey', bg = 'bg0' }
'ui.linenr.selected' = { fg = 'fg', bg = 'bg1' }
'ui.statusline' = { bg = 'bg1' }
'ui.statusline.inactive' = { bg = 'bg0' }
## Cursor and selection style
'ui.selection' = { bg = 'bg3' }
'ui.selection.primary' = { bg = 'bg4' }
'ui.cursor' = { fg = 'black', bg = 'grey' }
'ui.cursor.insert' = { fg = 'fg', bg = 'grey' }
'ui.cursor.select' = { fg = 'fg', bg = 'bg3' }
'ui.cursor.primary' = { fg = 'black', bg = 'fg' }
'ui.cursor.match' = { fg = 'black', bg = 'grey' }
## alternatively selection and cursor style
# 'ui.selection' = { fg = 'bg0', bg = 'purple' }
# 'ui.selection.primary' = { fg = 'black', bg = 'bg_blue' }
# 'ui.cursor' = { fg = 'bg0', bg = 'yellow' }
# 'ui.cursor.insert' = { fg = 'orange', bg = 'orange' }
# 'ui.cursor.select' = { fg = 'fg', bg = 'orange' }
# 'ui.cursor.primary' = { fg = 'bg0', bg = 'orange' }
# 'ui.cursor.match' = { fg = 'fg', bg = 'bg_blue' }
#
'ui.help' = { bg = 'bg4' }
'ui.highlight' = { bg = 'bg4' }
'ui.virtual' = { fg = 'bg4' }
'ui.virtual.ruler' = { bg = 'black' }
'ui.virtual.whitespace' = { fg = 'bg4' }
'ui.virtual.indent-guide' = { fg = 'black' }
## Popups
# code action
'ui.menu' = { fg = 'fg', bg = 'bg2' }
'ui.menu.selected' = { fg = 'bg0', bg = 'green' }
# docs
'ui.popup' = { bg = 'black' }
'ui.popup.info' = { fg = 'fg', bg = 'bg1' }
#
'tag' = { fg = 'green' }
'label' = { fg = 'purple' }
# TODO? is module
'module' = { bg = 'orange' }
'special' = { fg = 'orange' }
'operator' = { fg = 'red' }
# TODO? is property
'property' = { fg = 'blue' }
# TODO? is attribute and attributes
'attribute' = { fg = 'purple' }
'attributes' = { fg = 'purple' }
'namespace' = { fg = 'blue', modifiers = ['italic'] }
'type' = { fg = 'orange', modifiers = ['italic'] }
'type.builtin' = { fg = 'red' }
'type.enum.variant' = { fg = 'purple' }
'constructor' = { fg = 'blue' }
'constant' = { fg = 'orange' }
'constant.builtin' = { fg = 'purple' }
'constant.character' = { fg = 'green' }
'constant.character.escape' = { fg = 'blue' }
'constant.numeric' = { fg = 'blue' }
## Strings
'string' = { fg = 'yellow' }
'string.regexp' = { fg = 'blue' }
'string.special' = { fg = 'orange' }
'string.special.path' = { fg = 'orange' }
'string.special.url' = { fg = 'orange' }
'string.special.symbol' = { fg = 'orange' }
## Comments
'comment' = { fg = 'grey', modifiers = ['italic'] }
'comment.line' = { fg = 'grey', modifiers = ['italic'] }
'comment.block' = { fg = 'grey', modifiers = ['italic'] }
'comment.block.documentation' = { fg = 'grey', modifiers = ['italic'] }
#
'variable' = { fg = 'fg' }
'variable.builtin' = { fg = 'blue', modifiers = ['italic'] }
'variable.parameter' = { fg = 'fg' }
'variable.other.member' = { fg = 'fg' }
'variable.function' = { fg = 'blue' }
'punctuation' = { fg = 'fg' }
'punctuation.delimiter' = { fg = 'fg' }
'punctuation.bracket' = { fg = 'fg' }
## Keywords
'keyword' = { fg = 'red' }
'keyword.directive' = { fg = 'blue' }
'keyword.control' = { fg = 'red' }
'keyword.function' = { fg = 'red' }
'keyword.control.conditional' = { fg = 'red' }
'keyword.control.repeat' = { fg = 'red' }
'keyword.control.import' = { fg = 'red' }
'keyword.control.return' = { fg = 'red' }
'keyword.control.exception' = { fg = 'red' }
## Functions
'function' = { fg = 'green' }
'function.builtin' = { fg = 'blue', modifiers = ['italic'] }
'function.method' = { fg = 'blue' }
'function.macro' = { fg = 'purple' }
'function.special' = { fg = 'blue' }
## Markup - docs popup
'markup.heading' = { fg = 'green' }
'markup.heading.1' = { fg = 'yellow' }
'markup.heading.2' = { fg = 'orange' }
'markup.heading.3' = { fg = 'red' }
'markup.heading.4' = { fg = 'green' }
'markup.heading.5' = { fg = 'blue' }
'markup.heading.6' = { fg = 'purple' }
'markup.heading.marker' = { fg = 'orange' }
'markup.list' = { fg = 'green' }
'markup.list.numbered' = { fg = 'green' }
'markup.list.unnumbered' = { fg = 'green' }
'markup.bold' = { modifiers = ['bold'] }
'markup.italic' = { modifiers = ['italic'] }
'markup.link' = { fg = 'blue' }
'markup.link.url' = { fg = 'blue' }
'markup.link.label' = { fg = 'blue' }
'markup.link.text' = { fg = 'blue' }
'markup.quote' = { fg = 'blue' }
'markup.normal' = { fg = 'blue' }
'markup.normal.completion' = { bg = 'bg0' }
'markup.normal.raw' = { bg = 'bg0' }
'markup.heading.completion' = { fg = 'greenN' }
'markup.heading.raw' = { bg = 'bg0' }
'markup.raw' = { bg = 'bg0' }
'markup.raw.block' = { bg = 'bg1', fg = 'orange' }
'markup.raw.inline' = { fg = 'blue' }
'markup.raw.inline.completion' = { fg = 'green' }
'markup.raw.inline.hover' = { fg = 'green' }
## Diffs
'diff.plus' = { bg = 'diff_green' }
'diff.minus' = { bg = 'diff_red' }
'diff.delta' = { bg = 'diff_blue' }
'diff.delta.moved' = { bg = 'diff_yellow' }

[palette]
# Primary colors
'red' = '$red'
'green' = '$green'
'yellow' = '$yellow'
'blue' = '$blue'
'purple' = '$purple'
'orange' = '$orange'
# Base colors, sorted from darkest to lightest
'black' = '$black'
'bg0' = '$bg0'
'bg1' = '$bg1'
'bg2' = '$bg2'
'bg3' = '$bg3'
'bg4' = '$bg4'
'grey_dim' = '$grey_dim'
'grey' = '$grey'
'fg' = '$fg'
# Extra variants
'bg_red' = '$bg_red'
'diff_red' = '$diff_red'
'bg_green' = '$bg_green'
'diff_green' = '$diff_green'
'bg_blue' = '$bg_blue'
'diff_blue' = '$diff_blue'
'diff_yellow' = '$diff_yellow'
" > ./build/helix/sono_kaizen_$flavor.toml