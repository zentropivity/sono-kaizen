#!/usr/bin/fish
mkdir -p ./build/kitty

echo "background $bg0
foreground $fg

cursor $fg
cursor_text_color $black
selection_foreground $bg0
selection_background $fg

# black
color0 $black
# light black
color8 $grey

# red
color1 $red
# light red
color9 $red

# green
color2 $green
# light green
color10 $green

# yellow
color3 $yellow
# light yellow
color11 $yellow

# orange
color4 $orange
# light blue
color12 $orange

# magenta
color5 $purple
# light magenta
color13 $purple

# blue
color6 $blue
# light cyan
color14 $blue

# white
color7 $fg
# bright white
color15 $fg" > ./build/kitty/sono_kaizen_$flavor.conf