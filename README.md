# Sono Kaizen Theme

The aim of this repository to generate Sonokai theme for other applications.

## Latest

The latest build is available for download as CI artifacts [here](https://gitlab.com/zentropivity/sono-kaizen/-/jobs).

## Usage

The script expects to be run from the top level folder of the repository and it is written in fish so that has to be installed.

`gen.fish -f/flavor [flavor name] -a/app [application]`

The following flavors are available: default, shusia, andromeda, atlantis, maia, espresso.  
Take a look in [generators](./generators) for which applications are supported.  
After running, the generated theme files can be found in `./build`.

## Why rename?

Because there may be differences in theme, like syntax highlighting, so I cannot say that its Sonokai truly.  
'Sono Kaizen' (その 改善) translates to 'The Improvement'. 🔼

## License

Except where noted (below and/or in individual files), all code in this repository is licensed under:

- MIT License ([LICENSE-MIT](LICENSE-MIT) or [http://opensource.org/licenses/MIT](http://opensource.org/licenses/MIT))

Note: the original [Sonokai theme](https://github.com/sainnhe/sonokai) is licensed under MIT.
